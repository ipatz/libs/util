package util

import (
	"net"
	"net/url"
	"os"
	"strings"
)

// IsErrorTimeout will retrieve err and check that err has Timeout error type or not
func IsErrorTimeout(err error) bool {
	istimeout := false
	switch err := err.(type) {
	case net.Error:
		if err.Timeout() {
			//		fmt.Println("This was a net.Error with a Timeout")
			istimeout = true
		}
	case *url.Error:
		//	fmt.Println("This is a *url.Error")
		if err, ok := err.Err.(net.Error); ok && err.Timeout() {
			//		fmt.Println("and it was because of a timeout")
			istimeout = true
		}
	}
	if strings.Contains(err.Error(), "Client.Timeout") {
		istimeout = true
	}
	if os.IsTimeout(err) {
		istimeout = true
	}
	return istimeout
}
